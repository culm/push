/*
 * 
 */
package com.intasect.push.service;

import java.net.InetSocketAddress;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.IBinder;
import android.os.Message;

import com.intasect.push.R;
import com.intasect.push.TestActivity;
import com.intasect.push.handle.PushClient;
import com.intasect.push.utils.Const;

/**
 * 
 * @author zengjiantao
 * @date 2013-4-9
 */
public class PushService extends Service {

	private PushClient mClient;

	private Handler mHandler;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		initHandler();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		mClient = new PushClient(new InetSocketAddress("192.168.1.2", 9999),
				mHandler);
		mClient.start();
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onDestroy() {
		if (mClient != null) {
			mClient.disConnect();
		}
		super.onDestroy();
	}

	/**
	 * 初始化Handler
	 */
	private void initHandler() {
		mHandler = new Handler(new Callback() {

			@Override
			public boolean handleMessage(Message msg) {
				switch (msg.what) {
				case Const.PUSH_MSG:
					String pushMsg = msg.obj.toString();
					showNotification(pushMsg);
					break;

				default:
					break;
				}
				return false;
			}
		});
	}

	/**
	 * 
	 * 在状态栏显示通知
	 */

	@SuppressWarnings("deprecation")
	private void showNotification(String msg) {
		// 创建一个NotificationManager的引用
		NotificationManager notificationManager = (NotificationManager) getSystemService(android.content.Context.NOTIFICATION_SERVICE);

		// 定义Notification的各种属性
		Notification notification = new Notification(R.drawable.ic_launcher,
				msg, System.currentTimeMillis());

		// FLAG_AUTO_CANCEL 该通知能被状态栏的清除按钮给清除掉
		// FLAG_NO_CLEAR 该通知不能被状态栏的清除按钮给清除掉
		// FLAG_ONGOING_EVENT 通知放置在正在运行
		// FLAG_INSISTENT 是否一直进行，比如音乐一直播放，知道用户响应
		notification.flags |= Notification.FLAG_AUTO_CANCEL; // 表明在点击了通知栏中的"清除通知"后，此通知不清除，经常与FLAG_ONGOING_EVENT一起使用

		// DEFAULT_ALL 使用所有默认值，比如声音，震动，闪屏等等
		// DEFAULT_LIGHTS 使用默认闪光提示
		// DEFAULT_SOUND 使用默认提示声音
		// DEFAULT_VIBRATE 使用默认手机震动，需加上<uses-permission
		// android:name="android.permission.VIBRATE" />权限
		notification.defaults = Notification.DEFAULT_SOUND;

		// 设置通知的事件消息
		CharSequence contentTitle = msg; // 通知栏标题
		CharSequence contentText = msg; // 通知栏内容
		Intent notificationIntent = new Intent(this, TestActivity.class); // 点击该通知后要跳转的Activity

		PendingIntent contentItent = PendingIntent.getActivity(this, 0,
				notificationIntent, 0);
		notification.setLatestEventInfo(this, contentTitle, contentText,
				contentItent);
		// 把Notification传递给NotificationManager
		notificationManager.notify(0, notification);
	}

}
