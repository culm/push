package com.intasect.push;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.intasect.push.service.PushService;

public class HomeActivity extends Activity {

	private Button mConnect;

	private Button mDisConnect;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home);
	}

	@Override
	protected void onResume() {
		super.onResume();
		mConnect = (Button) findViewById(R.id.connect_btn);
		mDisConnect = (Button) findViewById(R.id.dis_connect_btn);
		mConnect.setOnClickListener(mOnclickListener);
		mDisConnect.setOnClickListener(mOnclickListener);
	}

	private final OnClickListener mOnclickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.connect_btn:
				startService(new Intent(HomeActivity.this, PushService.class));
				break;
			case R.id.dis_connect_btn:
				stopService(new Intent(HomeActivity.this, PushService.class));
				break;

			default:
				break;
			}
		}

	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.home, menu);
		return true;
	}
}
